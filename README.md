# Here is my simple scripts for some wargames and CTFs

```solver.py``` - simple usage of r2pipe binding for python. Solve [ELF C++ 0 protection](https://www.root-me.org/en/Challenges/Cracking/ELF-C-0-protection) from [RootMe](https://rootme.org) wargame  
```ch35_sol.py``` - simple usage of pwnlib. Solve [Basic Overflow x64](https://www.root-me.org/en/Challenges/App-System/ELF-x64-Stack-buffer-overflow-basic) from [RootMe](https://rootme.org) wargame  
