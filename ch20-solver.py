import idc
import sys

rol = lambda val, r_bits, max_bits: \
	(val << r_bits%max_bits) & (2**max_bits-1) | \
	((val & (2**max_bits-1)) >> (max_bits-(r_bits%max_bits)))
  
ror = lambda val, r_bits, max_bits: \
	((val & (2**max_bits-1)) >> r_bits%max_bits) | \
	(val << (max_bits-(r_bits%max_bits)) & (2**max_bits-1))
 
 
def get_lo_byte(x):
	return x & 0xff
 
def add_cl_eax(x, y):
	return (x - get_lo_byte(x)) + ((get_lo_byte(x) + y) & 0xff)
 
def get_key():
	start	= 0x08048080
	end		= 0x08048123
	buf 	= idc.get_bytes(start, end-start, False)
	result = 0
	for i in range(0,len(buf)):
		result = add_cl_eax(result, ord(buf[i]))
		result = rol(result,3,32) & 0xffffffff
	return result
 
a = get_key()
i = 25
key_addres = 0x08049155
imp = idc.get_bytes(key_addres, i, False)
imp = imp[::-1]
result = ""

for each in imp:
	a = ror(a,1,32)
	x = a & 0xff
	result += str(chr(x ^ ord(each)))
	i -= 1

print result[::-1]