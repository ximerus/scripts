import r2pipe
import sys

def solver():
    r2 = r2pipe.open("/root/ch25", ["-d"])
    r2.cmd("s 0x08048a9d")
    r2.cmd("wao un-cjmp")
    r2.cmd("db 0x08048a18")
    sys.stdout.write("Password is:\t")
    for i in range(0, 48):
        r2.cmd("dc")
        sys.stdout.write(chr(int(r2.cmd("dr al"), 16)))
    sys.stdout.write("\n")

if __name__ == "__main__":
    solver()
